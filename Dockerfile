FROM microsoft/azure-cli:latest

COPY pipe/*.sh /
RUN chmod a+x /*.sh
ENTRYPOINT ["/pipe.sh"]
