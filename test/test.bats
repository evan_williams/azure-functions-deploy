#!/usr/bin/env bats

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/azure-function-deploy:latest"}

  echo "Building image..."
  run docker build -t ${DOCKER_IMAGE} .

  az login --service-principal --username ${AZURE_APP_ID} --password ${AZURE_PASSWORD} --tenant ${AZURE_TENANT_ID}

  RESOURCE_GROUP="azure-functions-deploy-test-${BITBUCKET_BUILD_NUMBER}"
  FUNCTION_APP="azure-functions-deploy-test-app-${BITBUCKET_BUILD_NUMBER}"

  az group create --name ${RESOURCE_GROUP} --location australiaeast

  az group deployment create \
    -g ${RESOURCE_GROUP} \
    --template-file ${BATS_TEST_DIRNAME}/deploy.json \
    --parameters FunctionApp_Name="${FUNCTION_APP}"
}

teardown() {
    echo "Teardown happens after each test."
    az group delete -n ${RESOURCE_GROUP} -y  --no-wait
}

@test "Deploy Function" {
  run docker run \
    -e AZURE_APP_ID=${AZURE_APP_ID} \
    -e AZURE_TENANT_ID=${AZURE_TENANT_ID} \
    -e AZURE_PASSWORD=${AZURE_PASSWORD} \
    -e ZIP_FILE=${BATS_TEST_DIRNAME}/EchoFunction.zip \
    -e FUNCTION_APP_NAME=${FUNCTION_APP} \
    -v $(pwd):$(pwd) \
    -w $(pwd):$(pwd) \
    ${DOCKER_IMAGE}

  echo ${output}
  [ "$status" -eq 0 ]

  result=$(curl -q -w "%{http_code}" https://${FUNCTION_APP}.azurewebsites.net/api/echo)
  [ "${result}" -eq "204" ]
}

