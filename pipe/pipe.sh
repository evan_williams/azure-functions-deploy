#!/usr/bin/env bash
#
# ms
#
# Required globals:
#   NAME
#
# Optional globals:
#   DEBUG (default: "false")

source "$(dirname "$0")/common.sh"

info "Executing the pipe..."

enable_debug() {
  if [[ "${DEBUG}" == "true" ]]; then
    info "Enabling debug mode."
    set -x
  fi
}
enable_debug

# required parameters
FUNCTION_APP_NAME=${FUNCTION_APP_NAME:?'FUNCTION_APP_NAME variable missing.'}
ZIP_FILE=${ZIP_FILE:?'ZIP_FILE variable missing.'}

# default parameters
DEBUG=${DEBUG:="false"}

AZURE_APP_ID=${AZURE_APP_ID:?'AZURE_APP_ID variable missing.'}
AZURE_PASSWORD=${AZURE_PASSWORD:?'AZURE_PASSWORD variable missing.'}
AZURE_TENANT_ID=${AZURE_TENANT_ID:?'AZURE_TENANT_ID variable missing.'}

    # auth
AUTH_ARGS_STRING="--username ${AZURE_APP_ID} --password ${AZURE_PASSWORD} --tenant ${AZURE_TENANT_ID}"

if [[ "${DEBUG}" == "true" ]]; then
AUTH_ARGS_STRING="${AUTH_ARGS_STRING} --debug"
fi

AUTH_ARGS_STRING="${AUTH_ARGS_STRING} ${EXTRA_ARGS:=""}"

debug AUTH_ARGS_STRING: "${AUTH_ARGS_STRING}"

info "Signing in..."

az login --service-principal ${AUTH_ARGS_STRING}


# Lookup resource group.
RESOURCE_GROUP_NAME=$(az resource list -n "${FUNCTION_APP_NAME}" --resource-type "Microsoft.Web/Sites" --query '[0].resourceGroup' | cut -d '"' -f 2)
if [[ -z $RESOURCE_GROUP_NAME ]];
then
    fail "Could not get the resource group associated with function ${FUNCTION_APP_NAME}."
fi

info "Deploying zip file"
az functionapp deployment source config-zip  -g "${RESOURCE_GROUP_NAME}" -n "${FUNCTION_APP_NAME}" --src "${ZIP_FILE}"

if [[ "$?" == "0" ]]; then
  success "Success!"
else
  fail "Error!"
fi
